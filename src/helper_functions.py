import numpy as np
import pandas as pd
import os
import pydicom
from pydicom.dataset import Dataset, FileDataset

EMPTY = 'NONE'
FAILED = 'FAILED'
SUCCESS = 'SUCCESS'


def save_single_file(args, data, file_name, pat_id):
    if args.save_as.upper() == 'DICOM':
        if not os.path.exists(f"{args.output_folder}/{'DICOM'}"):
            os.makedirs(f"{args.output_folder}/{'DICOM'}")
        save_DICOM(data, f"{args.output_folder}/DICOM/{file_name}.dcm", pat_id=pat_id,
                      sampling_rate=int(args.new_sampling_rate), duration_sec=args.duration_in_sec)
    elif args.save_as.upper() == 'CSV':
        if not os.path.exists(f"{args.output_folder}/{'CSV'}"):
            os.makedirs(f"{args.output_folder}/{'CSV'}")
        np.savetxt(f"{args.output_folder}/CSV/{file_name}.csv", data, delimiter=",")

    elif args.save_as.upper() == 'NUMPY':
        if not os.path.exists(f"{args.output_folder}/{'NUMPY'}"):
            os.makedirs(f"{args.output_folder}/{'NUMPY'}")
        np.save(f"{args.output_folder}/NUMPY/{file_name}.npy", data)
    else:
        raise Exception('Unknown file type: specify --save_as\nOptions are dicom, csv, numpy\nDefault is dicom')

def save_DICOM(ecg_data, file_path, pat_id, sampling_rate, duration_sec):
    # Set up the File Meta Information
    file_meta = Dataset()
    file_meta.MediaStorageSOPClassUID = '1.2.840.10008.5.1.4.1.1.7'
    file_meta.MediaStorageSOPInstanceUID = pydicom.uid.generate_uid()
    file_meta.ImplementationClassUID = '1.2.3.4'
    file_meta.TransferSyntaxUID = '1.2.840.10008.1.2'

    # Create a DICOM dataset
    ds = FileDataset(file_path, {}, file_meta=file_meta, preamble=b"\0" * 128)

    # Set required DICOM tags
    ds.PatientName = "-"
    ds.PatientID = str(pat_id)
    ds.sampling_rate = int(sampling_rate)
    ds.duration_in_sec = duration_sec
    # dataset.StudyDate = datetime.datetime.now().strftime("%Y%m%d")
    # dataset.StudyTime = datetime.datetime.now().strftime("%H%M%S")
    # Set the transfer syntax
    ds.is_little_endian = True
    ds.is_implicit_VR = True

    # Add ECG data
    ds.BitsStored = 16
    ds.SamplesPerPixel = 1
    ds.PixelRepresentation = 0  # Unsigned integer
    ds.PixelData = ecg_data.tobytes()
    ds.Rows, ds.Columns = ecg_data.shape

    # Save the DICOM file
    ds.save_as(file_path)


def setup_patient_permutation(args, all_files):
    # Permutation
    if os.path.exists(args.permute_patients):
        permutation = pd.read_csv(args.permute_patients, index_col=0)
        permute = True
    elif args.permute_patients.isdigit():
        if int(args.permute_patients) != 1:
            np.random.seed(int(args.permute_patients))
        permutation_list = pd.DataFrame(zip(all_files, np.random.permutation(len(all_files))), columns=['Filename', 'ID'])
        i = 1
        while os.path.exists(f"{args.output_folder}/patient_permutation_{i}.csv"):
            i += 1
        permutation_list.to_csv(f'{args.output_folder}/patient_permutation_{i}.csv')
        permutation = pd.read_csv(f'{args.output_folder}/patient_permutation_{i}.csv', index_col=0)
        permute = True
    else:
        permutation = False
        permute = False
    return permutation, permute


def setup_checkpoint(args, log_columns):
    log_columns_default = ['-'] * len(log_columns)
    if args.checkpoint == EMPTY:
        log_dict = pd.DataFrame(columns=log_columns)
        j = 1
        while os.path.exists(f"{args.output_folder}/data_extraction_summary_{j}.csv"):
            j += 1
        checkpoint_path = f"{args.output_folder}/data_extraction_summary_{j}.csv"
    else:
        checkpoint_path = args.checkpoint
        log_dict = pd.read_csv(checkpoint_path, index_col=0)
    return checkpoint_path, log_dict, log_columns_default