from src.matlab import read_pdf_via_matlab_minimal as read_pdf_2, read_pdf_via_matlab as read_pdf

import numpy as np
import pickle
import matplotlib.pyplot as plt

EMPTY = 'NONE'
FAILED = 'FAILED'
SUCCESS = 'SUCCESS'


def extract_data_via_matlab(source_folder, source_file, destination_folder, interpolate=False, verbose=False):
    destination = f'{destination_folder}/{source_file.split(".")[0]}.p'
    source = source_folder + "/" + source_file

    try:
        if not interpolate:
            print(f"Running MATLAB")
            r_pdf = read_pdf_2.initialize()
            raw_data = r_pdf.read_pdf_no_interpolate(source)
            r_pdf.terminate()
            # print(raw_data.keys())
            if len(raw_data.keys()) == 0:
                print(f"{source_file} returned nothing")
                return "none", "EMPTY_RETURN"
            print("MATLAB Done")
        else:
            r_pdf = read_pdf.initialize()
            raw_data = r_pdf.read_pdf_adjust(source, 1)
            r_pdf.terminate()
    except:
        print(f"{source_file} failed with interpolation set to {interpolate}")
        return "none", FAILED

    if verbose:
        num_pages = int(len(raw_data.keys()) / 2)

        for i in range(num_pages):
            plt.subplot(num_pages, 1, i+1)
            num_channels = len(raw_data[f'ecg_p{i+1}'])
            np_ecg = np.array(raw_data[f'ecg_p{i+1}'])
            np_time = np.array(raw_data[f'time_p{i+1}'])
            for j in range(num_channels):
                ecg_channel = np.trim_zeros(np_ecg[j, 0], 'b')
                time = np_time[j][0, :len(ecg_channel)]
                strange_criterium = sum(time[:-1] > time[1:])

                if strange_criterium < 10:
                    plt.plot(np_time[j][0, :len(ecg_channel)], ecg_channel)

        plt.suptitle(source_file)
        plt.show()

    pickle.dump(raw_data, open(destination, 'wb'))

    return destination, SUCCESS


def process_matlab_output(source_folder, source_file, interpolation_args, verbose=False):
    source = source_folder + "/" + source_file
    raw_data = pickle.load(open(source, 'rb'))

    # check number of pages
    if len(raw_data.keys()) > 4:
        print(f"{source_file}: Too many pages. Currently only ECGs are supported with a maximum of 2 pages.")
        return np.nan, FAILED, -1

    # filter out incomplete ecgs
    for i, key in enumerate(raw_data.keys()):
        if np.array(raw_data[key]).shape[0] % 6 != 0:
            print(f"{source_file}: strange number of channels for {key}: {np.array(raw_data[key]).shape}")
            break
            # return FAILED

    data = {"ecg": [], "time": []}
    for i in range(len(raw_data.keys())//2):
        data["ecg"] += raw_data[f"ecg_p{i+1}"]
        data["time"] += raw_data[f"time_p{i+1}"]

    interpolate_data, duration_in_sec, return_code = interpolate_pickle(data, **interpolation_args)

    if return_code != SUCCESS:
        return np.nan, return_code, duration_in_sec

    bad_index = []
    for lead in range(interpolate_data['ecg'].shape[0]):
        if len(np.unique(interpolate_data['ecg'][lead])) < 10:
            bad_index += [lead]
        else:
            interpolate_data['ecg'][lead] = interpolate_data['ecg'][lead] - np.mean(interpolate_data['ecg'][lead])
    interpolate_data['ecg'] = np.delete(interpolate_data['ecg'], bad_index, axis=0)
    interpolate_data['duration'] = np.delete(interpolate_data['duration'], bad_index, axis=0)
    print(interpolate_data['ecg'].shape)

    if interpolate_data['ecg'].shape[0] != 12:
        return np.nan, 'BAD_CHANNEL_NUM', duration_in_sec

    if verbose:
        for i in range(12):  # len(interpolate_data['ecg'])
            plt.plot(interpolate_data['ecg'][i]-50*i)
        plt.title(source_file)
        plt.show()
    return interpolate_data, SUCCESS, duration_in_sec


def interpolate_pickle(raw_data, new_sampling_rate, svg_time_units=-1, duration_in_sec=-1, on_none_unique_time_use='last'):

    durations = []
    for i in range(len(raw_data['ecg'])):
        # get duration for each channel
        # TODO: think if it is necessary to trim zeros from the front
        time = np.array(raw_data['time'][i])[0]
        durations += [round(np.max(time) - time[0], 2)]
    unique_durations = np.unique(np.array(durations))
    min_duration = np.floor(np.min(unique_durations))

    if svg_time_units != -1:
        duration_in_sec = min_duration / svg_time_units
    elif duration_in_sec == -1:
        raise Exception('Cannot interpolate without information on time. Add eiter: --duration_in_sec or --svg_time_units')

    if not len(unique_durations) <= 2:
        print(f"WARNING: To many unique durations: {unique_durations}")
        return -1, duration_in_sec, 'TO_MANY_UNIQUE_DURATIONS'

    new_size = int(duration_in_sec * new_sampling_rate)

    interpolate_data = {'ecg': np.zeros((len(raw_data['ecg']), new_size)),
                        'time': np.zeros((len(raw_data['ecg']), new_size)),
                        'duration': np.zeros((len(raw_data['ecg'])))}

    for i in range(len(raw_data['ecg'])):
        # interpolate each channel

        time = np.array(raw_data['time'][i])[0]
        ecg = np.array(raw_data['ecg'][i])[0]

        time = np.trim_zeros(time, 'b')
        ecg = np.trim_zeros(ecg, 'b')

        if not len(time) == len(ecg):
            print(f"WARNING: time and data are of different length: {len(time), len(ecg)}")
            return -1, duration_in_sec, 'DIFFERENT_LENGTH'

        time = time - time[0]

        # spot duplicate times
        time_difference = time[1:] - time[:-1]

        # test if there are none unique time values
        if len(np.unique(time) < len(time)):
            if on_none_unique_time_use == 'first':
                unique_time_index = np.arange(1, len(time_difference) + 1)[time_difference != 0]
            elif on_none_unique_time_use == 'last':
                unique_time_index = np.arange(len(time_difference))[time_difference != 0]
            else:
                raise Exception(f'The time values are not unique for {i}. Specify arg on_none_unique_time_use.'
                                f' Options: "last", "first"')
        else:
            unique_time_index = np.arange(len(time_difference))

        unique_time_ecg = ecg[unique_time_index]
        # unique_time_ecg = unique_time_ecg - np.mean(unique_time_ecg)

        unique_time = time[unique_time_index]
        # new_size = int(np.max(time) - time[0])

        time_interpolate = np.linspace(0, min_duration, new_size)
        ecg_interpolate = np.interp(time_interpolate, unique_time, unique_time_ecg)

        interpolate_data['ecg'][i] = ecg_interpolate
        interpolate_data['time'][i] = time_interpolate
        interpolate_data['duration'][i] = durations[i]

    return interpolate_data, duration_in_sec, SUCCESS