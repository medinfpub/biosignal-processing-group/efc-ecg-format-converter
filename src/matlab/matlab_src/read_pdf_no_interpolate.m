function [record] = read_pdf_no_interpolate(fname)
% This is the adjusted MATLAB script for extracting the raw SVG coordinates from PDFs containing ECGs
%
%   Build Python Package from this file:
%   compiler.build.pythonPackage('read_pdf_no_interpolate.m','PackageName','read_pdf_via_matlab', 'OutputDir', './');
%
% REQUIREMENTS

% UNIX/ GNU/LINUX
%   A - This function works with UNIX systems with Inkscape
%     To read the header information and to extract the path of ECG leads,
%     this function is converting the PDF to SVG using 'Inkscape':
%     https://inkscape.org
%
%   B - Alternatively you can also use these two functions:
%
%     To read the header information the function is converting the PDF to
%     TXT using 'pdf2txt' written by Yusuke Shinyama and available with
%     'PDFMiner':
%     http://manpages.ubuntu.com/manpages/focal/man1/pdf2txt.1.html
%     https://pypi.org/project/pdfminer/
%
%     To extract the path of ECG leads, the function is converting the PDF
%     to SVG using 'pdf2svg' written by David Barton and Matthew Flaschen:
%     http://manpages.ubuntu.com/manpages/focal/man1/pdf2svg.1.html
%
% WINDOWS
%     To read the header information and to extract the path of ECG leads,
%     this function is converting the PDF to SVG using 'Inkscape':
%     https://inkscape.org
%
% MAC OS X
%     To read the header information and to extract the path of ECG leads,
%     this function is converting the PDF to SVG using 'Inkscape':
%     https://inkscape.org
%
%
%
% INPUT
% fname - file location
%
% OUTPUT
% record - Matlab struct containing the header information as named fields
% and the ECG is stored in record.ecg
%
%
%
%   MIT License (MIT) Copyright (c) 2020 Marcus Vollmer,
%   marcus.vollmer@uni-greifswald.de
%   Feel free to contact me for discussion, proposals and issues.
%   last modified: 12 October 2020
%   version: 0.02
%

verbose = false;
ink = [];

record = struct();
tmp_dir = 'tmpconvert';
mkdir(tmp_dir);

status_1 = 1;
status_2 = 1;
status_3 = 1;

% Check neccessary installations and convert PDF
switch isunix + 1*ismac + 3*ispc
  case 1
% UNIX

    status_1 = system('command -v pdf2svg');
    status_2 = system('command -v pdf2txt');
    status_3 = system('command -v inkscape');

    % For testing puposes
    if ~isempty(ink)
        % force Inkscape usage
        status_2 = 2;
    end

    if (status_1==0 && status_2==0)
        curtxt = [tmp_dir filesep 'tmp.txt'];
        copyfile(fname,[tmp_dir filesep 'tmp.pdf']);
        system(['pdf2txt -o ' curtxt ' ' tmp_dir filesep 'tmp.pdf']);
        system(['pdf2svg ''' fname ''' ' tmp_dir filesep 'tmp_%03i.svg all']);
    elseif status_3==0
        system(['cp ''' fname ''' ' tmp_dir filesep 'tmp.pdf']);
        system(['inkscape --export-plain-svg --export-type=svg -T ' tmp_dir filesep 'tmp.pdf -o ' tmp_dir filesep 'tmp.svg']);
        movefile([tmp_dir filesep 'tmp.svg'],[tmp_dir filesep 'tmp.txt']);
        i = 0;
        e = '';
        while ~contains(e, 'Import first page instead.')
            i = i+1;
            [~,e] = system(['inkscape --export-plain-svg --export-type=svg --pdf-page=' num2str(i) ' ' tmp_dir filesep 'tmp.pdf -o ' tmp_dir filesep sprintf('tmp_%03i.svg', i)]);
        end
        delete([tmp_dir filesep sprintf('tmp_%03i.svg', i)]);
    end

  case 2
% MAC OS
    [status_3, cmdout] = system('mdfind -name ''kMDItemFSName=="Inkscape.app"''');

    if ~isempty(cmdout)
        copyfile(fname,[tmp_dir filesep 'tmp.pdf']);
        system(['/Applications/Inkscape.app/Contents/MacOS/inkscape --export-plain-svg --export-type=svg -T ' tmp_dir filesep 'tmp.pdf -o ' tmp_dir filesep 'tmp.svg']);
        movefile([tmp_dir filesep 'tmp.svg'],[tmp_dir filesep 'tmp.txt']);
        i = 0;
        e = '';
        while ~contains(e, 'Import first page instead.')
            i = i+1;
            [~,e] = system(['/Applications/Inkscape.app/Contents/MacOS/inkscape --export-plain-svg --export-type=svg --pdf-page=' num2str(i) ' ' tmp_dir filesep 'tmp.pdf -o ' tmp_dir filesep sprintf('tmp_%03i.svg', i)]);
        end
        delete([tmp_dir filesep sprintf('tmp_%03i.svg', i)]);
    end

  case 3
% WINDOWS
    [status_3] = system('inkscape --version');

    if status_3==0
        copyfile(fname,[tmp_dir filesep 'tmp.pdf']);
        system(['inkscape --export-plain-svg --export-type=svg -T ' tmp_dir filesep 'tmp.pdf -o ' tmp_dir filesep 'tmp.svg']);
        movefile([tmp_dir filesep 'tmp.svg'],[tmp_dir filesep 'tmp.txt']);
        i = 0;
        e = '';
        while ~contains(e, 'Import first page instead.')
            i = i+1;
            [~,e] = system(['inkscape --export-plain-svg --export-type=svg --pdf-page=' num2str(i) ' ' tmp_dir filesep 'tmp.pdf -o ' tmp_dir filesep sprintf('tmp_%03i.svg', i)]);
        end
        delete([tmp_dir filesep sprintf('tmp_%03i.svg', i)]);
    end

end


if ~(status_1==0 && status_2==0 || status_3==0)
    error('The import of ECG-PDFs requires the installation of `Inkscape` or the packages `pdf2svg` and `pdf2txt`.')
end


% Read pages to extract ECG
% Remarks:
% Inkscape paths example (Move with implicit LineTo (relative coordinates)):
% m 25.61887,56.09067 0.13848,0.0023 0.13848,-0.0042
% PDF2SVG paths example (Move with explicit LineTo (absolute coordinates)):
% M 25.617188 56.089844 L 25.757812 56.09375 L 25.894531 56.089844
% Read more here:
% https://css-tricks.com/svg-path-syntax-illustrated-guide/
% https://www.w3.org/TR/SVG/paths.html

pages = dir([tmp_dir filesep '*.svg']);

% record.ecg = {};
% record.time = {};

num_pages = size(pages,1);

for j=1:size(pages,1)

    cursvg = [tmp_dir filesep pages(j).name];
    fid = fopen(cursvg);
    if fid == -1
        break
    end
    data = fscanf(fid,'%c');
    fclose(fid);

  % extract ECG
  % Apple Watch /
  % tested with iOS 13.6, watchOS 6.2.8, Watch5,2
  % tested with iOS 13.6, watchOS 5.3.3, Watch4,3
    front_hit = strfind(data,'<path');
    back_hit_candidates = strfind(data,'/>');
    back_hit = front_hit;
    for jj=1:size(front_hit,2)
        back_hit(jj) = back_hit_candidates(sum(back_hit_candidates<front_hit(jj))+1);
    end

    [out,idx] = sort(back_hit-front_hit, 'descend');

    if length(out) == 0
        continue;
    end

    num_paths = sum(out/out(1)>.5);
    idx = sort(idx(1:num_paths));

    time = [];
    ecg = [];
    lengths = [];
    xtmp = [];
    for jj=1:num_paths
        cur_path = data(front_hit(idx(jj)):back_hit(idx(jj)));
        [mat_path, path_syntax] = path2mat(cur_path);
        x = mat_path(1,:);
        y = mat_path(2,:);
        if x(1)==x(2)
            x = x(2:end);
            y = y(2:end);
        end

        lengths(jj) = length(x);
        time(jj,1:length(x)) = x;
        ecg(jj,1:length(y)) = y;
        xtmp = [xtmp x];
    end

  % Linear interpolation of missing coordinates
    xtmp = sort(unique(x));
    d_xtmp = diff(xtmp);
    xtmp = xtmp([true d_xtmp>.1*mean(d_xtmp)]);
    d_xtmp = diff(xtmp);
    skip_coord = round(d_xtmp/min(d_xtmp));
    tmp_coord = find(skip_coord~=1);

    xtmp_interp = [];
    for j_skip_coord = 1:length(tmp_coord)
        skip_length = skip_coord(tmp_coord(j_skip_coord));
        skip_pos = tmp_coord(j_skip_coord);
        xtmp_interp = [xtmp_interp linspace(xtmp(skip_pos), xtmp(skip_pos+1), skip_length+1)];
    end
    xtmp = unique(sort([xtmp xtmp_interp]));


    if path_syntax=='M'
      % Search for optimal pair of location and scale to convert double to integer
        jj=1;
        a = round(1/min(setdiff(abs(diff(ecg(jj,1:lengths(jj)))),0)));
        ecg = ecg*a;

        for jj=1:num_paths
            b = mean(ecg(jj,1:lengths(jj))-round(ecg(jj,1:lengths(jj))));
            ecg(jj,1:lengths(jj)) = round(ecg(jj,1:lengths(jj))-b);
        end
    end

    % record.ecg = {};
    % record.time = {};

    for jj=1:num_paths
        % record.ecg = [record.ecg ecg(j, :)];
        % record.time = [record.time time(j, :)];
        % record.ecg = cat(1, record.ecg, ecg(j,:))
        % record.time = cat(1, record.time, time(j,:))
        if verbose
            jj
            length(ecg(jj, :))
            length(time(jj, :))
            record.ecg(j, :) = ecg(jj, :)
            record.time(j, :) = time(jj, :)
        else
            % record.ecg{j, jj} = ecg(jj, :);
            % record.time{j, jj} = time(jj, :);
            ecg_key = "ecg_p" + j;
            time_key = "time_p" + j;
            record.(ecg_key){jj} = ecg(jj, :);
            record.(time_key){jj} = time(jj, :);

    end

    % record.ecg = record.ecg-round((max(record.ecg)+min(record.ecg))/2);

    %plot(record.ecg)
    % fclose(fid);
end

% size(record.ecg{1})

%{
% Disabeled because record shape changed to keep individual pages
if ~isempty(record) && 0
  % Remove leading and trailing NaNs and adjust annotations
    nansum = sum(isnan(record.ecg), 1)==size(record.ecg,1);
    nanidx = find(nansum);
    if ~isempty(nanidx)
        leadingnans  = sum(nanidx==1:length(nanidx));
        trailingnans = sum(fliplr(nanidx)==size(record.ecg,2):-1:(size(record.ecg,2)-length(nanidx)+1));

        record.ecg = record.ecg(:, (leadingnans+1):(size(record.ecg,2)-trailingnans));
        if ~isempty(record.ann)
            record.ann = record.ann-leadingnans;
        end
    end
end
%}


end
% Remove temporary folder
rmdir(tmp_dir, 's');
