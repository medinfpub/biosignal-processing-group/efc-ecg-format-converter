import pandas as pd
import numpy as np
import pydicom

'''
TODO:
- dicom.errors.InvalidDicomError: File is missing DICOM File Meta Information header or the 'DICM' prefix is missing from the header
'''

path = "../data/InterTAK ECGs/Done/DICOM/id-0_hz-200_size-2000.dcm"

res = pydicom.dcmread(path, force=True)
print(res)
# print(res.)


def create_permutation_list(path):
    permutation = pd.read_csv(path, index_col=0)
    id_ = np.random.permutation(np.arange(0, len(permutation)))
    permutation['ID'] = id_
    print(permutation)
    print(permutation[permutation['Filename'] == 'Zuerich101']['ID'][0])
    # permutation.to_csv('./')


