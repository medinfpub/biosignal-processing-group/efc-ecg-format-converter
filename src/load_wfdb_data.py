import helper_functions as hf

import numpy as np
import argparse
# import wfdb
import os
import struct
import pickle


def decode_binary_file(file_path):
    samples = []

    with open(file_path, 'rb') as file:
        while True:
            # Read 2 bytes (16 bits) from the file
            data = file.read(2)

            if not data:
                # End of file reached
                break

            # Unpack the 16-bit signed integer using little-endian byte order
            sample = struct.unpack('<h', data)[0]

            # Append the decoded sample to the list
            samples.append(sample)

    return samples


def reshape_decoded_data(decoded_data):

    decoded_data = np.array(decoded_data)
    num_of_channels = decoded_data[2]
    meta_data = decoded_data[:12]

    data = decoded_data[num_of_channels:]
    data = data.reshape((num_of_channels, len(data) // num_of_channels), order='F')
    return data, meta_data, num_of_channels


def load_wfdb_ecgs(args, all_files, permutation, permute):

    # used, if all input files shall be stored in a single output file
    wfdb_ecgs = {}

    print(f"Processing {len(all_files)} WFDB files")
    for i, filename in enumerate(all_files):
        if i % 100 == 0:
            print(f"{np.round(100 * i / len(all_files))}%", end='\r')

        if filename[-4:] == '.hea':
            print('skipping', filename)
            continue
        file_path = args.input_folder + '/' + filename.split('.')[0]  # + '.mat'
        # signals, fields = wfdb.rdsamp(file)
        # channel_names = ['I', 'II', 'III', 'aVR', 'aVL', 'aVF', 'V1', 'V2', 'V3', 'V4', 'V5', 'V6']

        binary_file_path = file_path + '.mat'
        decoded_samples = decode_binary_file(binary_file_path)
        data, meta_data, num_of_channels = reshape_decoded_data(decoded_samples)

        if permute:
            try:
                pat_id = permutation[permutation['Filename'] == filename]['ID'].values[0]
            except Exception:
                raise Exception(
                    f'Patient permutation list is not complete. Missing {filename}\nPerhaps create a new list with --permute_patients')
        else:
            pat_id = filename[:-4]

        file_name = f"id-{pat_id}"
        if args.save_as.upper() == 'SINGLE_FILE':
            wfdb_ecgs[pat_id] = data
        else:
            hf.save_single_file(args, data, file_name, pat_id)

    if args.save_as.upper() == 'SINGLE_FILE':
        with open(f"{args.output_folder}/data.p", 'wb') as pickle_file:
            pickle.dump(wfdb_ecgs, pickle_file)

    return


def parse_args():
    parser = argparse.ArgumentParser(description="Extract data from WFDB")

    # Define command-line arguments and options
    parser.add_argument("--input_folder", type=str, default=0, help="Enter the input folder, in which the WFDB is.")
    parser.add_argument("--output_folder", type=str, default=0, help="Enter the output folder, where the results shall be stored.")
    parser.add_argument("--permute_patients", type=str, default='no', help="Enter '1' to generate a permutation file with random seed, enter a number to generate a permutation file with a set seed. Enter a file path to a csv list with a permutation index")
    parser.add_argument("--verbose", type=bool, default=0, help="Print additional Information")
    parser.add_argument("--save_as", type=str, default='single_file', help="Save extracted data as DICOM/csv/numpy/single_file")

    args = parser.parse_args()
    return args


if __name__ == "__main__":
    args = parse_args()

    all_files = [filename for filename in os.listdir(args.input_folder)]  # [:10]

    permutation, permute = hf.setup_patient_permutation(args, all_files)

    print('Processing WFDB')
    load_wfdb_ecgs(args, all_files, permutation, permute)
    print('DONE')
