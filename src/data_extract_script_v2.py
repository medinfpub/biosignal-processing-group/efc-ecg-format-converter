import helper_functions as hf
import pdf_data_extractor as pdf_extractor

import numpy as np
import pandas as pd
import os
import argparse

EMPTY = 'NONE'
FAILED = 'FAILED'
SUCCESS = 'SUCCESS'
# SKIPPED = 'SKIPPED'

'''
Version: 2

Last change: 15.12.2023
Author: Sebastian Schmale, 
Requires Matlab script by Marcus Vollmer

How to use:
See Readme.md

'''


def parse_args():
    parser = argparse.ArgumentParser(description="Extract ECG data from PDFs")

    # Define command-line arguments and options
    parser.add_argument("--input_type", type=str, default=EMPTY, help="TODO")
    parser.add_argument("--input_folder", type=str, default=EMPTY, help="Folder of input file/s")
    parser.add_argument("--output_folder", type=str, default=EMPTY, help="Storage location of the processing output")
    parser.add_argument("--single_file", type=str, default=EMPTY, help="For processing just a single file")
    parser.add_argument("--save_as", type=str, default='dicom', help="Save extracted data as DICOM/csv/numpy")
    parser.add_argument("--permute_patients", type=str, default='no', help="Enter '1' to generate a permutation file with random seed, enter a number to generate a permutation file with a set seed. Enter a file path to a csv list with a permutation index")
    parser.add_argument("--svg_time_units", type=float, default=-1, help="When dealing with varying length of ecg. Calculate via: hidden_duration / duration_in_seconds")
    parser.add_argument("--duration_in_sec", type=float, default=-1, help="If all ecgs have the same duration in seconds, enter this")
    parser.add_argument("--new_sampling_rate", type=int, default=200, help="Interpolate to the sampling rate")
    parser.add_argument("--verbose", type=bool, default=0, help="Print additional Information")
    parser.add_argument("--keep_temp_files", type=bool, default=0, help="Dont delete temp files")
    parser.add_argument("--checkpoint", type=str, default=EMPTY, help="Path to log file if it already exists. Skip files which are listed in the provided file")

    args = parser.parse_args()
    return args


if __name__ == "__main__":
    args = parse_args()

    # Create a list of all files
    if args.single_file != EMPTY:
        all_files = [args.single_file]
    else:
        # loop through every file in the source folder and add it to list
        all_files = [filename for filename in os.listdir(args.input_folder)]

    failed = []
    # 'ecgs' is used if all input files shall be stored in a single output file
    ecgs = {}

    # Create folder to store temp files
    temp_folder = './temp_data_extraction'
    if not os.path.exists(temp_folder):
        os.makedirs(temp_folder)

    permutation, permute = hf.setup_patient_permutation(args, all_files)

    log_columns = ['patient_id', 'code_matlab', 'code_interpolation', 'original_type', 'final_type',
                   'intermediate_type', 'min_duration_in_svg', 'max_duration_in_svg', 'sampling_rate',
                   'duration_in_sec']
    checkpoint_path, log_dict, log_columns_default = hf.setup_checkpoint(args, log_columns)

    for i, filename in enumerate(all_files):
        input_folder = args.input_folder
        file_type = filename.split(".")[-1]
        file_id = filename.split(".")[0]

        if permute:
            try:
                pat_id = permutation[permutation['Filename'] == filename]['ID'].values[0]
            except Exception:
                raise Exception(f'Patient permutation list is not complete. Missing {filename}\nPerhaps create a new list with --permute_patients')
        else:
            pat_id = file_id

        if pat_id in log_dict['patient_id'].values:
            print(f"\nSkipping {i}/{len(all_files)}: {filename}")
            continue
        print(f"\nProcessing {i}/{len(all_files)}: {filename}")

        new_entry = pd.DataFrame([log_columns_default], columns=log_columns)

        new_entry['patient_id'] = pat_id
        new_entry['original_type'] = file_type
        interpolate = False

        if file_type == 'pdf':
            # convert pdf to raw_data_points
            dest, return_code = pdf_extractor.extract_data_via_matlab(args.input_folder, filename, temp_folder, verbose=args.verbose)
            # set file_type to .p
            if return_code == SUCCESS:
                # continue to process this file
                input_folder = temp_folder
                new_entry['intermediate_type'] = 'pickle'
                interpolate = True
            new_entry['code_matlab'] = return_code

            if interpolate:
                interpolation_args = {'on_none_unique_time_use': 'last',
                                      'new_sampling_rate': args.new_sampling_rate,
                                      'svg_time_units': args.svg_time_units,
                                      'duration_in_sec': args.duration_in_sec}

                ecg_data, return_code, duration_in_sec = pdf_extractor.process_matlab_output(input_folder, file_id + '.p', interpolation_args, verbose=args.verbose)

                new_entry['code_interpolation'] = return_code
                new_entry['duration_in_sec'] = np.round(duration_in_sec, 3)

                if not args.keep_temp_files and return_code == SUCCESS:
                    os.remove(input_folder + "/" + file_id + '.p')

                if return_code == SUCCESS:

                    std_duration = np.round(np.max(ecg_data['duration']), 3)
                    min_duration = np.round(np.min(ecg_data['duration']), 3)
                    new_entry['max_duration_in_svg'] = std_duration
                    new_entry['min_duration_in_svg'] = min_duration
                    new_entry['final_type'] = args.save_as.upper()
                    new_entry['sampling_rate'] = args.new_sampling_rate

                    file_name = f"id-{pat_id}_hz-{int(args.new_sampling_rate)}_size-{ecg_data['ecg'].shape[-1]}"

                    hf.save_single_file(args, ecg_data['ecg'], file_name, pat_id)

                else:
                    new_entry['final_type'] = 'pickle'
                    failed += [filename]

        # TODO: handel different file types in this script
        elif file_type.upper == 'CSV':
            print('TODO: csv')
        elif file_type.upper == 'WFDB':
            print("Use the script 'load_wfdb_data.py' to extract WFDB data")
        elif file_type.upper == 'XML':
            print("Use Philips ECG parser to extract XML data")
        else:
            print("Unrecognized file type")

        log_dict = pd.concat([log_dict, new_entry], ignore_index=True)
        log_dict.to_csv(checkpoint_path)

    # Get the information on Success rate out of the log file
    # print(failed)
    # print("Failed: ", len(failed), " - Total: ", len(all_files))

    if len(os.listdir(temp_folder)) == 0:
        os.remove(temp_folder)

    print("DONE")
