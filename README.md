# EFC-ECG-Format-Converter

![](img/data_pipline.png)

This Tool converts PDFs and WFDB containing ECGs into more research-friendly data formats.

> [workshop-biosignal](https://workshop-biosignale.de/)

## Usage

python data_extract_script_v2.py --input_folder .../data/ecgs
--output_folder .../data/out --duration_in_sec 10

python load_wfdb_data.py --input_folder .../data/ecgs
--output_folder .../data/out

## Setup

1. Clone Repository,
2. install requirements.txt, 
3. add PATH requirements.

PATH requirements:
- [MatLab runtime version 9.14](https://www.mathworks.com/help/compiler/install-the-matlab-runtime.html) (licence not required)
- [Inkscape](https://inkscape.org/en/download/)

So far only tested on Ubuntu.

## General 

        --input_folder: type=str,   default=EMPTY,  
                Description: Folder of input file/s  
        --output_folder:    type=str,   default=EMPTY,  
                Description: Storage location of the processing output  
        --save_as:  type=str,   default='dicom'  
                Description: "Save extracted data as DICOM/csv/numpy  
        --permute_patients: type=str,   default='no'  
                Description: "Enter '1' to generate a permutation file with random seed, enter a number to generate a permutation file with a set seed. Enter a file path to a csv list with a permutation index  
        --verbose:  type=bool,  default=0,  
                Description: Print additional Information  

## Command line options for PDF

        --duration_in_sec:  type=float, default=-1,  
                Description: If all ECGs have the same duration in seconds, enter the duration  
        --svg_time_units:   type=float, default=-1,  
                Description: When dealing with varying length of ECG. Calculate via: hidden_duration / duration_in_seconds  
        --new_sampling_rate:    type=int,   default=200,  
                Description: Interpolate to the sampling rate  
        --keep_temp_files:  type=bool,  default=0,  
                Description: Dont delete temp files  
        --checkpoint:   type=str,   default=EMPTY,  
                Description: Path to log file if it already exists. Skip files which are listed in the provided file  
        --single_file:  type=str,   default=EMPTY,  
                Description: For processing just a single file  

---

## Notes on PDF extraction:

Problem with sampling rate:
- Interpolation needs to know how many data points it shall produce (Size so far: 2000 / 876)
- The matlab script only gives the duration in the form of svg coordinates (~ 326 and 708)
- We need to know real time duration in seconds or at least:
    - The translation factor from the arbitrary timescale of the pdf (310? and 708?)
    - to duration in seconds: 708 d_{svg} / 10sec = ~70 d_{svg}/sec     310 d_{svg} / 4.4sec = ~70 d_{svg}/sec
Conclusion:
- We pass the desired sampling rate and the d_{svg}/sec argument 
- => calculate the time: 708 d_{svg} / 70 d_{svg}/sec = ~10 sec
- => calculate the size of the nparray: 10sec * 200Hz = 2000 
Further Problem:
- The Channels have varying length for the single page files:
- => as before use the min_duration of all channels per file

## To dos

Short term Todos:
- Test run with everything
- Integrate data extraction for xml with: https://github.com/tbj128/philips-ecg-parser/tree/main
- pseudonymization with a larger range of IDs (not just number of patients)

Long term Todos:
- Extract y-Axis measuring units
- make load_wfdb_data more universal
- Deal with "check number of pages"
